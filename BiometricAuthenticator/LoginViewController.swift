//
//  LoginViewController.swift
//  BiometricAuthenticator
//
//  Created by Sushmitha Ganesh on 7/16/20.
//  Copyright © 2020 Test. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var loginButton: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func evaluatePolicy(context: LAContext) -> Bool {
        var error: NSError?
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthentication, error: &error) {
            if let error = error {
                let canEvaluateErrorCode = LAError(_nsError: error)
                switch canEvaluateErrorCode.code {
                case .userFallback:
                    print("Can evaluate fallback error")
                @unknown default:
                    print("Can evaluate policy")
                }
            }
            
            else {
            
            context.evaluatePolicy(LAPolicy.deviceOwnerAuthentication, localizedReason: "Authenticate to view your account!") { (success, error) in
                print(success)
                if let error = error {
                    let evaluateErrorCode = LAError(_nsError: error as NSError)
                    switch evaluateErrorCode.code {
                    case .userFallback:
                        print("User fall back")
                    case .passcodeNotSet:
                        let passcodeNotSetAlert = UIAlertController(title: "Set a Passcode", message: "Add a passcode to proceed further ", preferredStyle: .alert)
                        passcodeNotSetAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    case .touchIDLockout:
                       let passcodeNotSetAlert = UIAlertController(title: "Sign in to view", message: "Add a passcode to proceed further ", preferredStyle: .alert)
                                               passcodeNotSetAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    case .invalidContext:
                        print("Create new context")
                    case .authenticationFailed:
                        print("Authentication Failed")
                    @unknown default:
                        print("Default Error")
                    }
                }
                }
            }
            }
            return true
            
        } /*else {
            print("Can't evaluate test policy")
            print(error?.localizedDescription ?? "NO error description")
            return false */
        
    
    
    @IBAction func loginButtonTouchedUpInside(_ sender: UIButton) {
        let context = LAContext()
        //context.localizedFallbackTitle = "Use Passcode" 
        if !evaluatePolicy(context: context)  {
            // alert user to enroll
            
        } else {
            
        }
    }
}

